<?php

namespace App\Controller;

use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends AbstractController
{
    /**
     * @Route("/", name="books_homepage")
     * @param BookRepository $bookRepository
     * @return Response
     */
    public function index(BookRepository $bookRepository): Response
    {
        return $this->render('book/index.html.twig', [
            'books' => $bookRepository->findBy(['status' => true]),
        ]);
    }

    /**
     * @Route("/book/{id}", name="book_details", methods="GET")
     * @param Request $request
     * @param BookRepository $bookRepository
     * @return Response
     */
    public function bookShow(Request $request, BookRepository $bookRepository): Response
    {
        $id = $request->attributes->get('id');
        $book = $bookRepository->findOneBy([
            'id' => $id, 'status' => true,
        ]);

        if (!$book) {
            throw $this->createNotFoundException('The book does not exist');
        }

        return $this->render('book/show.html.twig', ['book' => $book]);
    }
}
